#!/bin/bash -e
date=20170301
base=https://dumps.wikimedia.org/fiwiktionary/$date/

cd /import

for table in $(cat tables); do
  echo $table
  wget --continue -q -O ${table}.sql.gz $base/fiwiktionary-$date-${table}.sql.gz
done

wget --continue -q -O dump.xml.bz2 \
  $base/fiwiktionary-${date}-pages-articles-multistream.xml.bz2
