#!/bin/bash -e
cd /usr/src/other/mwoffliner
node mwoffliner.js \
     --adminEmail root@example.com \
     --mwUrl http://${WIKI_CONTAINER:-mediawiki}/ \
     --mwWikiPath /index.php \
     --mwApiPath /api.php
# --verbose
