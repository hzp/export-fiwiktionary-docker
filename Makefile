all: mw db

mw:
	docker build -t mw -f mw.df .

db:
	docker build -t mwdb -f db.df .

zim:
	docker build -t zim -f zim.df .

mwdumper:
	docker build -t mwdumper -f mwdumper.df .

mwdumper.jar:
	docker run --rm mwdumper cat /usr/src/mwdumper/target/mwdumper-1.25.jar >$@.tmp
	mv $@.tmp $@

.PHONY: all mw zim mwdumper
