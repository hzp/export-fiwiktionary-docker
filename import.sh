#!/bin/bash -e
cd /import

mysql -h localhost -u root --password="$MYSQL_ROOT_PASSWORD" <my_wiki.sql
for table in dump $(cat tables); do
  echo $table
  gzip -dc <${table}.sql.gz \
    | mysql -h localhost -u root --password="$MYSQL_ROOT_PASSWORD" my_wiki
done
